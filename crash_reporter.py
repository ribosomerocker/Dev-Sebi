# !/usr/bin/python
# -*- coding: utf8 -*-

import discord

import json
import os

from sebimachine.utils.ioutils import in_here


class Crash_Reporter(discord.Client):
    def __init__(self):
        discord.Client.__init__(self, status=discord.Status.invisible)
        with open(in_here('sebimachine/config', 'PrivateConfig.json')) as fp:
            self.secrets = json.load(fp)

    async def on_ready(self):
        channel = self.get_channel(471618803256786944)

        msg = await channel.send('Last time the bot exited, it did it with a error. <@&479896018658394152>, please commit a fix,'
                                 ' then react to this message with "\N{OK HAND SIGN}" for the bot to reboot')

        await msg.add_reaction('\N{OK HAND SIGN}')

        def check(reaction, user):
            return reaction.message.id == msg.id and str(reaction) == '\N{OK HAND SIGN}' and user.id != self.user.id

        await self.wait_for('reaction_add', check=check)

        await channel.send('Rebooting...')
        # noinspection PyProtectedMember
        os._exit(0)


client = Crash_Reporter()

client.run(client.secrets['bot-key'])

