#!/bin/bash

# 20th June 2018
# Esp: added a trap here, as it otherwise attempts to restart when given
# the interrupt signal. This is really annoying over SSH when I have
# a 1-second lag anyway.

trap "echo 'Received interrupt. Exiting.'; exit 0" SIGINT SIGTERM

# Also loads the venv if it is present.
[ -d .venv/bin ] && source .venv/bin/activate && echo "Entered venv." || echo "No venv detected."

function update() {
    git pull --all
    python -m pip install -U -r requirements.txt
}

FAIL=0

while true; do
    if [ ${FAIL} -eq 1 ]; then
        update
        let FAIL=0
    fi

    # Just respawn repeatedly until sigint.
    python -m sebimachine

    EXIT_STATUS=${?}
    if [ ${EXIT_STATUS} -ne 0 ]; then
        python crash_reporter.py
        let FAIL=1
    fi

    # Added colouring to ensure the date of shutdown and the exit code stands
    # out from the other clutter in the traceback that might have been output.
    echo -e "\e[0;36m[$(date --utc)]\e[0m Sebi-Machine shutdown with error code \e[0;31m${EXIT_STATUS}\e[0m. Restarting..." >&1

    sleep 1
done
