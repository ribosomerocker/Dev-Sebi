# !/usr/bin/python
# -*- coding: utf8 -*-

"""
App entry point.

Something meaningful here, eventually.
"""

import asyncio
import json
import logging
import os
import sys
import traceback
import shutil
from typing import Dict
from libneko import embeds

from discord.ext import commands

from .config.config import LoadConfig
from .utils.ioutils import in_here
from .shared_libs import database
from .shared_libs.loggable import Loggable

from .utils.utils import is_contributor_check

# Init logging to output on INFO level to stderr.
logging.basicConfig(level="INFO", format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")

REBOOT_FILE = "sebimachine/config/reboot"

# If uvloop is installed, change to that eventloop policy as it
# is more efficient
try:
    # https://stackoverflow.com/a/45700730
    if sys.platform == "win32":
        loop = asyncio.ProactorEventLoop()
        asyncio.set_event_loop(loop)
        logging.warning("Detected Windows. Changing event loop to ProactorEventLoop.")
    else:
        import uvloop

        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        del uvloop
except BaseException as ex:
    logging.warning(
        f"Could not load uvloop. {type(ex).__qualname__}: {ex};",
        "reverting to default impl.",
    )
else:
    logging.info(f"Using uvloop for asyncio event loop policy.")


# Bot Class
# Might be worth moving this to it's own file?
class SebiMachine(commands.Bot, LoadConfig, Loggable):
    """This discord bot is dedicated to http://www.discord.gg/GWdhBSp"""

    def __init__(self):
        # Initialize and attach config / settings
        LoadConfig.__init__(self)
        commands.Bot.__init__(self, command_prefix=self.defaultprefix)
        with open(in_here("config", "PrivateConfig.json")) as fp:
            self.bot_secrets = json.load(fp)
        self.db_con = database.DatabaseConnection(**self.bot_secrets["db-con"])
        self.failed_cogs_on_startup = {}
        self.book_emojis: Dict[str, str] = {
            "unlock": "🔓",
            "start": "⏮",
            "back": "◀",
            "hash": "#\N{COMBINING ENCLOSING KEYCAP}",
            "forward": "▶",
            "end": "⏭",
            "close": "🇽",
        }

        # Load plugins
        # Add your cog file name in this list
        cogs = []
        with open(in_here("extensions.txt")) as cog_file:
            for line in cog_file.read().split('\n'):
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                if line.startswith('*'):
                    line = 'sebimachine.cogs.' + line[1:]
                cogs.append(line)

        for cog in cogs:
            try:
                self.load_extension(cog)
                self.logger.info(f"Loaded: {cog}")
            except BaseException as ex:
                logging.exception(f'Could not load {cog}', exc_info=(type(ex), ex, ex.__traceback__))
                self.failed_cogs_on_startup[cog] = ex

    async def on_ready(self):
        """On ready function"""
        self.maintenance and self.logger.warning("MAINTENANCE ACTIVE")
        if os.path.exists(REBOOT_FILE):
            with open(REBOOT_FILE, "r") as f:
                reboot = f.readlines()
            if int(reboot[0]) == 1:
                await self.get_channel(int(reboot[1])).send(embed=embeds.Embed(
                    description="Sebi-Machine has restarted.",
                    colour=self.embed_color,
                    timestamp=None))
                for cog, ex in self.failed_cogs_on_startup.items():

                    try:
                        tb = ''.join(traceback.format_exception(type(ex), ex, ex.__traceback__))[-1500:]
                        channel = self.get_channel(471618803256786944)
                        await channel.send(f'FAILED TO LOAD {cog} BECAUSE OF {type(ex).__name__}: {ex}')

                        pag = commands.Paginator(prefix='```py', suffix='```')
                        for line in tb.split('\n'):
                            pag.add_line(line)

                        for page in pag.pages:
                            await channel.send(page)
                    except Exception:
                        self.logger.exception('Error occurred handling load error', exc_info=True)
        with open(REBOOT_FILE, "w") as f:
            f.write(f"0")

    async def on_message(self, message):
        # Make sure people can't change the username
        if message.guild:
            if message.guild.me.display_name != self.display_name:
                try:
                    await message.guild.me.edit(nick=self.display_name)
                except:
                    pass
        else:
            if (
                    "exec" in message.content
                    or "repl" in message.content
                    or "token" in message.content
            ) and message.author != self.user:
                await self.get_user(351794468870946827).send(
                    f"{message.author.name} ({message.author.id}) is using me "
                    f"in DMs\n{message.content}"
                )

        # If author is a bot, ignore the message
        if message.author.bot:
            return

        # Make sure the command get processed as if it was typed with lowercase
        # Split message.content one first space
        command = message.content.split(None, 1)
        if command:
            command[0] = command[0].lower()
            message.content = " ".join(command)
        message.content = " ".join(command)

        # process command
        await self.process_commands(message)

    async def on_guild_join(self, guild):
        await guild.leave()

class Reboot:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(brief="Reboots the bot", hidden=True)
    async def reboot(self, ctx):
        """Closes the bot process and starts it again automatically"""
        if is_contributor_check(ctx) is False:
            return
        logging.info(f"Rebooting by order from `{ctx.author}`")
        await ctx.send(embed=embeds.Embed(
            description="Sebi-Machine is restarting.",
            colour=self.bot.embed_color,
            timestamp=None))
        shutil.rmtree('sebimachine/config/reboot', ignore_errors=True)
        with open(f"sebimachine/config/reboot", "w") as f:
            f.write(f"1\n{ctx.channel.id}")
        # noinspection PyProtectedMember
        os._exit(0)


client = SebiMachine()

client.add_cog(Reboot(client))

client.run(client.bot_secrets["bot-key"])
