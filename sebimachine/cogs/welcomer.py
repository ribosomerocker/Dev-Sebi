#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import asyncio
from datetime import datetime, timedelta
import textwrap

import discord
from discord.ext import commands
from libneko import embeds


WELCOMER_CHANNEL_ID = 265828729970753537
GREEN_HUE = 0x51e723
RED_HUE = 0xfb3719


class Welcomer:
    def __init__(self ,bot):
        self.bot = bot
        
    
    @staticmethod
    def date_time_beautify(timestamp: datetime) -> str:
        """Outputs in the format YYYY-MM-DD at hh:mm:ss UTC"""
        return timestamp.strftime("%Y-%m-%d at %H:%M:%S %Z")
        
    @staticmethod
    def timespan_beautify(timespan: timedelta) -> str:
        total = int(timespan.total_seconds())
    
        mins, secs = divmod(total, 60)
        hours, mins = divmod(mins, 60)
        days, hours = divmod(hours, 24)
        months, days = divmod(days, 30)
        years, months = divmod(months, 12)

        bits = []
        years and bits.append(f'{years} year{years - 1 and "s" or ""}')
        months and bits.append(f'{months} month{months - 1 and "s" or ""}')
        days and bits.append(f'{days} day{days - 1 and "s" or ""}')
        hours and bits.append(f'{hours} hour{hours - 1 and "s" or ""}')
        mins and bits.append(f'{mins} minute{mins - 1 and "s" or ""}')
        secs and bits.append(f'{secs} second{secs - 1 and "s" or ""}')

        return ', '.join(bits[:3])

    async def on_member_join(self, member: discord.Member):
        """Handling a member join..."""
        welcomer_ch = self.bot.get_channel(WELCOMER_CHANNEL_ID)
        em = embeds.Embed(description=textwrap.dedent(f'''
            **Snowflake**: {member.id}
            **Mention**: @{member}
            **Account created at**: {self.date_time_beautify(member.created_at)}
        '''), colour=GREEN_HUE)
        
        em.set_author(name=f"{'BOT ' if member.bot else ''}{member} joined!", icon_url=member.guild.icon_url)
        em.set_footer(text=f'Member #{len(member.guild.members)} (of which {sum(m.bot for m in member.guild.members)} are bots)')
        em.set_thumbnail(url=member.avatar_url)
        
        await welcomer_ch.send(embed=em)


    async def on_member_remove(self, member: discord.Member):
        """Handling the a member removal..."""
        # Theory:
        # When a user gets nerfed, the audit logs do not update as fast as the bot does, so we
        # don't get the most up-to-date nerf record when we query them.
        # Fix:
        # Sleep for a bit, I guess.
        await asyncio.sleep(4)
        
        welcomer_ch = self.bot.get_channel(WELCOMER_CHANNEL_ID)
        
        # Unsure of whether empty reasons can ever be NoneType, and this is more symbolic.
        # NOT_APPLICABLE if this event didn't occur. Falsey if it did occur but no reason was given, else
        # a string explaining why.
        reason_type = 'left'
        reason = None
        
        # Check if banned or kicked
        try:
            async for entry in member.guild.audit_logs():
                created_at = entry.created_at
                
                # Couldn't decide between 10 (could rejoin in that time) and 5 (could fail if the bot is lagging).
                if created_at < datetime.utcnow() - timedelta(seconds=7.5):
                    break
                
                # target could be any object, it may not have an ID. (Cite: the docs)
                elif entry.target is not None and getattr(entry.target, 'id', -1) == member.id:
                    action = entry.action
                    
                    if action is discord.AuditLogAction.ban:
                        reason_type = 'ban'
                        reason = entry.reason
                        break
                    elif action is discord.AuditLogAction.kick:
                        reason_type = 'kick'
                        reason = entry.reason
                        break
            
        except discord.Forbidden:
            pass
        
        member_str = str(member) + (' a.k.a ' + member.nick if member.nick else '')
        
        if reason_type == 'left':
            formatted_msg = f"{member_str} just left!"
        elif reason_type == 'kick':
            # This is a valid synonym for kick, and it sounds funny. https://www.thesaurus.com/browse/kick
            formatted_msg = f'{member_str} was kicked!'
        elif reason_type == 'ban':
            formatted_msg = f'{member_str} was banned!'
        else:
            assert False, f'Unrecognised reason_type {reason_type}'
            
        em = embeds.Embed(
            title=formatted_msg, colour=RED_HUE,
            description=textwrap.dedent(f'''
                **Top role**: {member.top_role.mention if str(member.top_role) != '@everyone' else member.top_role}
                **Mention**: @{member}
                **Snowflake**: {member.id}
                **Account created at**: {self.date_time_beautify(member.created_at)}
                **Joined here at**: {self.date_time_beautify(member.joined_at)}
                **Was here for**: {self.timespan_beautify(datetime.utcnow() - member.joined_at)}
        '''))

        if reason is not None:
            em.description = (
                f'**Reason**: {reason}\n'
                + em.description
            )
        
        em.set_author(name=f"{'BOT ' if member.bot else ''}{member} left!", icon_url=member.guild.icon_url)
        em.set_footer(text=f'{len(member.guild.members)} members remain (of which {sum(m.bot for m in member.guild.members)} are bots)')
        em.set_thumbnail(url=member.avatar_url)
        
        await welcomer_ch.send(embed=em)


def setup(bot):
    bot.add_cog(Welcomer(bot))
